const { loginPage, userPage, getHome, registerAPI, registerPage } = require('../controller')
const router = require('express').Router()
const {passport} = require('../helper')
const {authentication, authorization} = require('../middleware')


router.get('/login', loginPage)
router.post('/login', passport.authenticate('local', {
    successRedirect : '/home',
    failureRedirect : '/login' 
}))
router.post('/register', registerAPI)
router.get('/register', registerPage)
router.get('/home', [authentication] ,getHome)
router.get('/user/:id', [authentication, authorization] ,userPage)


module.exports = router