const express = require('express')
const app = express()
const session = require('express-session')
const flash = require('express-flash')
const port = 4000
const {passport} = require('./helper')
const router = require('./routers')

app.use(express.urlencoded({extended : true}))

app.use(session({
    secret : '12345',
    resave : false,
    saveUninitialized : false
}))

app.set('view engine', 'ejs')

app.use(passport.initialize())
app.use(passport.session())

app.use(flash())

app.use(router)

app.listen(port, () => {
    console.log(`Run on port ${port}`)
})
