

async function authentication(req,res,next) {
    try {
        let session = req.session.passport
        console.log(session)

        if(session){
            next()
        } else {
            res.redirect('./login')
        }
    } catch (error) {
        res.redirect('./login')
    }
}

async function authorization(req,res,next){
    try {
        let session = req.session.passport
        let id = session.user
        let paramsId = req.params.id
        
        console.log(id, '<< ini id')
        if(+id === +paramsId){
            next()
        } else {
            res.redirect('./home')
        }
    } catch (error) {
        res.redirect('./login')
    }
}

module.exports = {
    authentication,
    authorization
}