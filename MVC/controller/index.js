const {users} = require('../models')


//View Controller
async function loginPage (req,res){
    try {
        res.render('./login')
    } catch (error) {
        res.send('error')
    }
}

async function getHome (req,res) {
    try {
        console.log(req.session)
        res.render('./home')
    } catch (error) {
        res.send('error')
    }
}

async function registerPage (req,res) {
    try {
        res.render('./register')
    } catch (error) {
        res.send('error')
    }
}

//Model Controller
async function loginAPI (req,res) {
    try {
        const {username, password} = req.body
        const loginData = await users.findOne({
            where : {
                username
            }
        })
        if(loginData.username === username && loginData.password === password){
            res.redirect('./home')
        }

    } catch (error) {
        res.redirect('./login')
    }
}

async function registerAPI(req,res) {
    try {
        const {username, password} = req.body
        console.log(username,password)
        const payload = {username, password}
        await users.create(payload)
        res.redirect('./login')
    } catch (error) {
        res.send('error')
    }
}

async function userPage (req,res) {
    try {
        let paramsId = req.params.id
        const data = await users.findOne({
            where : {
                id : paramsId
            }
        })
        //console.log(data)
        res.send(data)
    } catch (error) {
        res.send(error)
    }
}

module.exports = {
    loginPage,
    loginAPI,
    getHome,
    registerAPI,
    registerPage,
    userPage
}