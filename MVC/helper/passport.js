const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const {users} = require('../models')

passport.use(new LocalStrategy(async (username,password,done) => {
        console.log(username,password, '<< ini username & password')
        const loginData = await users.findOne({
            where : {
                username
            }
        })

        if(loginData.username === username && loginData.password === password){
            done(null, loginData)
        } else {
            done(null, null)
        }
}))

passport.serializeUser((users,done) => {
    console.log('===serializer===')
    console.log(users.id, '<< ini users')
    done(null, users.id)
})
passport.deserializeUser((users, done) => {
    console.log('===deserializer===')
    console.log(users, '<< ini users 2')
    done(null, {id : users})
})

module.exports = passport