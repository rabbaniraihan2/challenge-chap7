const {rooms,results,histories} = require('../models')
const jwt = require('jsonwebtoken')


async function loginAPI (req,res){
    try {
        const {player1} = req.body
        const loginPlayer = await rooms.findOne({
            where : {
                player1
            }
        })
        
        if(loginPlayer.player1 === player1){
            let token = jwt.sign(loginPlayer.player1, '123123')
            res.status(200).json({
                token
            })
        }
    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: 'Server error' });
    }
}

async function getRoom(req,res) {
    try {
        const {player1,player2} = req.body
        const payloadPlayer = {player1,player2}
        const player = await rooms.findOne({
            payloadPlayer
        })

        return res.status(200).json({
            player
        })
    } catch (error) {
        console.error(err);
        return res.status(500).json({ error: 'Server error' });
    }
    

}

async function createRoom (req,res) {
    try {
        const {player1,player2,choosenP1,choosenP2} = req.body
        const payloadRoom = {player1,player2,choosenP1,choosenP2}
        const dataRoom = await rooms.create(payloadRoom)

        const {choosenP1Id, choosenP2Id} = req.body
        const payloadResult = {choosenP1Id, choosenP2Id}
        await results.create(payloadResult)
        return res.status(201).json({dataRoom})
    } catch (err) {
        console.error(err);
        return res.status(500).json({ error: 'Server error' });
    }
}

async function fight (req,res) {
    try {
        let paramsId = req.params.id
        const data = await results.findOne({
            where : {
                id : paramsId
            }
        })

        const {player1,player2} = req.body
        const dataPlayer = await rooms.findOne({
            where : {
                player1,
                player2
            }
        })

        if(data.choosenP1Id === 'paper' && data.choosenP2Id === 'rock') {
            return res.status(200).json({
                result : `${dataPlayer.player1} Win`
            })
        } else if (data.choosenP1Id === 'paper' && data.choosenP2Id === 'scissors') {
            return res.status(200).json({
                result : `${dataPlayer.player2} Win`
            })
        } else if (data.choosenP1Id === 'paper' && data.choosenP2Id === 'paper') {
            return res.status(200).json({
                result : `Tied`
            })
        } else if (data.choosenP1Id === 'rock' && data.choosenP2Id === 'paper') {
            return res.status(200).json({
                result : `${dataPlayer.player2} Win`
            })
        } else if (data.choosenP1Id === 'rock' && data.choosenP2Id === 'scissors') {
            return res.status(200).json({
                result : `${dataPlayer.player1} Win`
            })
        } else if (data.choosenP1Id === 'rock' && data.choosenP2Id === 'rock') {
            return res.status(200).json({
                result : `Tied`
            })
        } else if (data.choosenP1Id === 'scissors' && data.choosenP2Id === 'paper') {
            return res.status(200).json({
                result : `${dataPlayer.player1} Win`
            })
        } else if (data.choosenP1Id === 'scissors' && data.choosenP2Id === 'rock') {
            return res.status(200).json({
                result : `${dataPlayer.player2} Win`
            })
        } else if (data.choosenP1Id === 'scissors' && data.choosenP2Id === 'scissors') {
            return res.status(200).json({
                result : `Tied`
            })
        } else {
            return res.status(404).json({
                result : 'Not Found'
            })
        }
    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: 'Server error' });
    }
}

module.exports = {
    createRoom,
    fight,
    loginAPI,
    getRoom
}