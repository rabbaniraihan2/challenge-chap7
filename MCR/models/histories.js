'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class histories extends Model {
    static associate(models) {
      histories.belongsTo(models.results, {foreignKey : 'resultId'})
    }
  }
  histories.init({
    resultId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'histories',
  });
  return histories;
};