'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class results extends Model {
    static associate(models) {
      results.belongsTo(models.rooms, {foreignKey : 'roomId'})
      results.belongsTo(models.rooms, {foreignKey : 'choosenP1Id'})
      results.belongsTo(models.rooms, {foreignKey : 'choosenP2Id'})
      results.hasOne(models.histories, {foreignKey : 'resultId'})
    }
  }
  results.init({
    choosenP1Id: DataTypes.STRING,
    choosenP2Id: DataTypes.STRING,
    result: DataTypes.STRING,
    roomId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'results',
  });
  return results;
};