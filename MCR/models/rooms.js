'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class rooms extends Model {
    static associate(models) {
      rooms.hasOne(models.results, {foreignKey : 'roomId'})
      rooms.hasOne(models.results, {foreignKey : 'choosenP1Id'})
      rooms.hasOne(models.results, {foreignKey : 'choosenP2Id'})
    }
  }
  rooms.init({
    player1: DataTypes.STRING,
    player2: DataTypes.STRING,
    choosenP1: DataTypes.STRING,
    choosenP2: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'rooms',
  });
  return rooms;
};