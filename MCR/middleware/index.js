const passport = require('passport')
const passportJWT = require('passport-jwt')
const JWTStrategy = passportJWT.Strategy
const ExtractJWT = passportJWT.ExtractJwt

passport.use(new JWTStrategy({
    jwtFromRequest : ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey : '123123'
}, (user, done) => {
    console.log(user, "<< ini adalah payload")
    return done(null, user)
}))

module.exports = {
    jwtPassport : passport
}
