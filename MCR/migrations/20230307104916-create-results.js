'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('results', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      choosenP1Id: {
        type: Sequelize.STRING
      },
      choosenP2Id: {
        type: Sequelize.STRING
      },
      result: {
        type: Sequelize.STRING
      },
      roomId: {
        type: Sequelize.INTEGER,
        references : {
          name : 'rooms-fk-at-results',
          model : 'rooms',
          field : 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('results');
  }
};