const passport = require('passport')
const { session } = require('passport')
const {createRoom, fight, loginAPI, getRoom} = require('../controller')
const router = require('express').Router()
const {jwtPassport} = require('../middleware')


router.post('/create-room', createRoom)
router.get('/fight/:id', fight)
router.post('/login', loginAPI)
router.get('/room',jwtPassport.authenticate('jwt', {session :false}) ,getRoom)

module.exports = router